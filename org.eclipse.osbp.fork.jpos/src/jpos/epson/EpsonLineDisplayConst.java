package jpos.epson;

public abstract interface EpsonLineDisplayConst {
	// Field descriptor #4 I
	public static final int DISP_DI_OUTPUT = 100;

	// Field descriptor #4 I
	public static final int DISP_DI_GRAPHIC = 101;

	// Field descriptor #4 I
	public static final int DISP_DI_SETLINESPACE = 102;

	// Field descriptor #4 I
	public static final int DISP_DI_SETFONT = 103;

	// Field descriptor #4 I
	public static final int DISP_DI_GETMODE = 104;

	// Field descriptor #4 I
	public static final int DISP_DI_GW_STYLE = 105;

	// Field descriptor #4 I
	public static final int DISP_DI_MODE_CHARACTER = 0;

	// Field descriptor #4 I
	public static final int DISP_DI_MODE_GRAPHICS = 1;

	// Field descriptor #4 I
	public static final int DISP_DI_FONT_A = 0;

	// Field descriptor #4 I
	public static final int DISP_DI_FONT_B = 1;

	// Field descriptor #4 I
	public static final int DISP_DI_GW_NORMAL = 0;

	// Field descriptor #4 I
	public static final int DISP_DI_GW_TRANSPARENT = 1;

	// Field descriptor #4 I
	public static final int DISP_DI_DUMMY = 0;

	// Field descriptor #4 I
	public static final int UPOS_EDISP_TOOMANYDEFGLYPH = 3001;

	// Field descriptor #4 I
	public static final int UPOS_EDISP_TOOMANYWINDOW = 3002;
}
