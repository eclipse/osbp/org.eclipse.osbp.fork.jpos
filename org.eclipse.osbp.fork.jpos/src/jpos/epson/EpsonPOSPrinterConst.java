package jpos.epson;

public abstract interface EpsonPOSPrinterConst {

	// Field descriptor #4 I
	public static final int PTR_DI_GET_SUPPORT_FUNCTION = 1;

	// Field descriptor #4 I
	public static final int PTR_DI_BINARY_CONVERSION = 2;

	// Field descriptor #4 I
	public static final int PTR_DI_CODE128_TYPE = 3;

	// Field descriptor #4 I
	public static final int PTR_DI_OUTPUT_NORMAL = 100;

	// Field descriptor #4 I
	public static final int PTR_DI_OUTPUT_REALTIME = 101;

	// Field descriptor #4 I
	public static final int PTR_DI_PANEL_SWITCH = 110;

	// Field descriptor #4 I
	public static final int PTR_DI_RECOVER_ERROR = 120;

	// Field descriptor #4 I
	public static final int PTR_DI_HARDWARE_RESET = 121;

	// Field descriptor #4 I
	public static final int PTR_DI_PRINT_FLASH_BITMAP = 200;

	// Field descriptor #4 I
	public static final int PTR_DI_PRINT_FLASH_BITMAP2 = 201;

	// Field descriptor #4 I
	public static final int PTR_DI_DELETE_NVIMAGE = 210;

	// Field descriptor #4 I
	public static final int PTR_DI_SELECT_SLIP = 300;

	// Field descriptor #4 I
	public static final int PTR_DI_SLIP_EMPHASIS = 310;

	// Field descriptor #4 I
	public static final int PTR_DI_LABEL_SET_PRINT_MODE = 400;

	// Field descriptor #4 I
	public static final int PTR_DI_LABEL_SET_COUNT_MODE = 401;

	// Field descriptor #4 I
	public static final int PTR_DI_LABEL_PRINT_COUNT = 402;

	// Field descriptor #4 I
	public static final int PTR_DI_LABEL_SET_COUNT_VALUE = 403;

	// Field descriptor #4 I
	public static final int PTR_DI_DELAYED_CUT = 500;

	// Field descriptor #4 I
	public static final int PTR_DI_CUT_AND_FEED_TOF = 501;

	// Field descriptor #4 I
	public static final int PTR_DI_RING_BUZZER = 600;

	// Field descriptor #4 I
	public static final int PTR_DI_GET_BATTERY_STATUS = 610;

	// Field descriptor #4 I
	public static final int PTR_DI_SELECT_PAGE_MODE = 700;

	// Field descriptor #4 I
	public static final int PTR_DI_SET_PAPERLAYOUT = 710;

	// Field descriptor #4 I
	public static final int PTR_DI_GET_PAPERLAYOUT = 711;

	// Field descriptor #4 I
	public static final int PTR_DI_OPERATION_MODE = 750;

	// Field descriptor #4 I
	public static final int PTR_DI_RING_BUZZER_WITH_TIME = 800;

	// Field descriptor #4 I
	public static final int PTR_DI_SET_BITMAP_PRINTING_TYPE = 1500;

	// Field descriptor #4 I
	public static final int PTR_DI_SOUND_MELODY = 1200;

	// Field descriptor #4 I
	public static final int PTR_DI_SET_SLIP_ROTATE_FONT_TYPE = 1700;

	// Field descriptor #4 I
	public static final int PTR_DI_PRINT_FRANKING = 1800;

	// Field descriptor #4 I
	public static final int PTR_DI_GET_OFFLINE_CONDITION = 1900;

	// Field descriptor #4 I
	public static final int PTR_DI_SELECT_SLIP_PAPER_TYPE = 2000;

	// Field descriptor #4 I
	public static final int PTR_DI_DUMMY = 0;

	// Field descriptor #4 I
	public static final int PTR_DI_VALIDATION = 1;

	// Field descriptor #4 I
	public static final int PTR_DI_EMPHASIS = 2;

	// Field descriptor #4 I
	public static final int PTR_DI_LABEL = 4;

	// Field descriptor #4 I
	public static final int PTR_DI_BLACK_MARK = 8;

	// Field descriptor #4 I
	public static final int PTR_DI_GB18030 = 16;

	// Field descriptor #4 I
	public static final int PTR_DI_PEELER = 32;

	// Field descriptor #4 I
	public static final int PTR_DI_BATTERY = 64;

	// Field descriptor #4 I
	public static final int PTR_DI_SOUTHASIA = 128;

	// Field descriptor #4 I
	public static final int PTR_DI_BC_NONE = 0;

	// Field descriptor #4 I
	public static final int PTR_DI_BC_NIBBLE = 1;

	// Field descriptor #4 I
	public static final int PTR_DI_BC_DECIMAL = 2;

	// Field descriptor #4 I
	public static final int PTR_DI_CODE_A = 0;

	// Field descriptor #4 I
	public static final int PTR_DI_CODE_B = 1;

	// Field descriptor #4 I
	public static final int PTR_DI_CODE_C = 2;

	// Field descriptor #4 I
	public static final int PTR_DI_DELETE_ALL = -1;

	// Field descriptor #4 I
	public static final int PTR_DI_SLIP_FULLSLIP = 0;

	// Field descriptor #4 I
	public static final int PTR_DI_SLIP_VALIDATION = 1;

	// Field descriptor #4 I
	public static final int PTR_DI_DISABLE_EMPHASIS = 0;

	// Field descriptor #4 I
	public static final int PTR_DI_ENABLE_EMPHASIS = 1;

	// Field descriptor #4 I
	public static final int PTR_DI_LABEL_RIGHT_SPACE = 0;

	// Field descriptor #4 I
	public static final int PTR_DI_LABEL_RIGHT_ZERO = 1;

	// Field descriptor #4 I
	public static final int PTR_DI_LABEL_LEFT_SPACE = 2;

	// Field descriptor #4 I
	public static final int PTR_DI_POWERED_BY_AC = 256;

	// Field descriptor #4 I
	public static final int PTR_DI_POWERED_BY_BATTERY = 512;

	// Field descriptor #4 I
	public static final int PTR_DI_BATTERY_FULL = 16;

	// Field descriptor #4 I
	public static final int PTR_DI_BATTERY_NEAR_MIDDLE = 128;

	// Field descriptor #4 I
	public static final int PTR_DI_BATTERY_MIDDLE = 8;

	// Field descriptor #4 I
	public static final int PTR_DI_BATTERY_NEAR_LOW = 64;

	// Field descriptor #4 I
	public static final int PTR_DI_BATTERY_LOW = 32;

	// Field descriptor #4 I
	public static final int PTR_DI_BATTERY_NEAR_EMPTY = 4;

	// Field descriptor #4 I
	public static final int PTR_DI_BATTERY_CLOSE_EMPTY = 2;

	// Field descriptor #4 I
	public static final int PTR_DI_BATTERY_REMOVED = 1;

	// Field descriptor #4 I
	public static final int PTR_DI_NORMAL_DOT_PAGE_MODE = 0;

	// Field descriptor #4 I
	public static final int PTR_DI_HALF_DOT_PAGE_MODE = 1;

	// Field descriptor #4 I
	public static final int PTR_DI_BITMAP_PRINTING_NORMAL = 0;

	// Field descriptor #4 I
	public static final int PTR_DI_BITMAP_PRINTING_MULTI_TONE = 1;

	// Field descriptor #4 I
	public static final int PTR_DI_SOUND_PATTERN_1 = 1;

	// Field descriptor #4 I
	public static final int PTR_DI_SOUND_PATTERN_2 = 2;

	// Field descriptor #4 I
	public static final int PTR_DI_SOUND_PATTERN_3 = 3;

	// Field descriptor #4 I
	public static final int PTR_DI_SOUND_PATTERN_4 = 4;

	// Field descriptor #4 I
	public static final int PTR_DI_SOUND_PATTERN_5 = 5;

	// Field descriptor #4 I
	public static final int PTR_DI_SOUND_PATTERN_ERROR = 100;

	// Field descriptor #4 I
	public static final int PTR_DI_SOUND_PATTERN_NOPAPER = 101;

	// Field descriptor #4 I
	public static final int PTR_DI_ROTATE_FONT_A = 0;

	// Field descriptor #4 I
	public static final int PTR_DI_ROTATE_FONT_B = 1;

	// Field descriptor #4 I
	public static final int PTR_DI_CONDITION_ONLINE = 0;

	// Field descriptor #4 I
	public static final int PTR_DI_CONDITION_RECEIPT_ONLY_OFFLINE = 1;

	// Field descriptor #4 I
	public static final int PTR_DI_CONDITION_SLIP_ONLY_OFFLINE = 2;

	// Field descriptor #4 I
	public static final int PTR_DI_CONDITION_OFFLINE_EXECUTE = 3;

	// Field descriptor #4 I
	public static final int PTR_DI_CONDITION_RECOVERBLE = 4;

	// Field descriptor #4 I
	public static final int PTR_DI_CONDITION_UNRECOVERBLE = 5;

	// Field descriptor #4 I
	public static final int PTR_DI_SLIP_PAPER_NORMAL = 1;

	// Field descriptor #4 I
	public static final int PTR_DI_SLIP_PAPER_COPY = 2;

	// Field descriptor #4 I
	public static final int PTR_DI_OPERATION_MODE_SERIAL = 0;

	// Field descriptor #4 I
	public static final int PTR_DI_OPERATION_MODE_PEEL_OFF = 1;

	// Field descriptor #4 I
	public static final int PTR_DIE_RESPONSE = 0;

	// Field descriptor #4 I
	public static final int PTR_DIE_SET_BITMAP_MODE = 110;

	// Field descriptor #4 I
	public static final int PTR_DIE_BUTTON_OPERATION = 210;

	// Field descriptor #4 I
	public static final int PTR_DIE_LABEL_REMOVAL = 211;

	// Field descriptor #4 I
	public static final int PTR_DIE_LABEL_REMOVE_OK = 212;

	// Field descriptor #4 I
	public static final int PTR_DIE_LABEL_JAM = 213;

	// Field descriptor #4 I
	public static final int PTR_DIE_MEMORY = 1;

	// Field descriptor #4 I
	public static final int PTR_DIE_VRAM = 2;

	// Field descriptor #4 I
	public static final int PTR_DIE_NVRAM = 3;

	// Field descriptor #4 I
	public static final int PTR_SUE_POWERED_BY_AC = 100001;

	// Field descriptor #4 I
	public static final int PTR_SUE_POWERED_BY_BATTERY = 100002;

	// Field descriptor #4 I
	public static final int PTR_SUE_BATTERY_FULL = 100003;

	// Field descriptor #4 I
	public static final int PTR_SUE_BATTERY_NEAR_MIDDLE = 100009;

	// Field descriptor #4 I
	public static final int PTR_SUE_BATTERY_MIDDLE = 100004;

	// Field descriptor #4 I
	public static final int PTR_SUE_BATTERY_NEAR_LOW = 100010;

	// Field descriptor #4 I
	public static final int PTR_SUE_BATTERY_LOW = 100011;

	// Field descriptor #4 I
	public static final int PTR_SUE_BATTERY_NEAR_EMPTY = 100005;

	// Field descriptor #4 I
	public static final int PTR_SUE_BATTERY_CLOSE_EMPTY = 100006;

	// Field descriptor #4 I
	public static final int PTR_SUE_BATTERY_OK = 100007;

	// Field descriptor #4 I
	public static final int PTR_SUE_BATTERY_REMOVED = 100008;

	// Field descriptor #4 I
	public static final int UPOS_EPTR_UNRECOVERABLE = 5001;

	// Field descriptor #4 I
	public static final int UPOS_EPTR_CUTTER = 5002;

	// Field descriptor #4 I
	public static final int UPOS_EPTR_MECHANICAL = 5003;

	// Field descriptor #4 I
	public static final int UPOS_EPTR_AUTOMATICAL = 5004;

	// Field descriptor #4 I
	public static final int UPOS_EPTR_LABEL_JAM = 5005;

	// Field descriptor #4 I
	public static final int UPOS_EPTR_LABEL_REMOVAL = 5006;

	// Field descriptor #4 I
	public static final int UPOS_EPTR_BUTTON_OPERATION = 5007;

	// Field descriptor #4 I
	public static final int UPOS_EPTR_POWER_OFF = 5008;

	// Field descriptor #4 I
	public static final int UPOS_EPTR_RESTART = 5009;

	// Field descriptor #4 I
	public static final int UPOS_EPTR_INITIALIZING = 5010;

	// Field descriptor #4 I
	public static final int UPOS_EPTR_CLEAR = 5011;

	// Field descriptor #4 I
	public static final int UPOS_EPTR_REMOVE_BUTTON = 5012;

	// Field descriptor #4 I
	public static final int UPOS_EPTR_FIRMWARE_UPDATING = 5013;

	// Field descriptor #4 I
	public static final int UPOS_EPTR_FIRMWARE_PROCESS = 5014;

	// Field descriptor #4 I
	public static final int PTR_BARCODE_TYPE_NONE = 0;

	// Field descriptor #4 I
	public static final int PTR_BARCODE_TYPE_1D = 1;

	// Field descriptor #4 I
	public static final int PTR_BARCODE_TYPE_2D = 2;

	// Field descriptor #4 I
	public static final int PTR_BARCODE_TYPE_TEXT = 3;

	// Field descriptor #4 I
	public static final int PTR_BARCODE_TYPE_COMPOSIT = 4;

	// Field descriptor #4 I
	public static final int PTR_BARCODE_TYPE_OTHER = 5;

	// Field descriptor #4 I
	public static final int PTR_BARCODE_TYPE_UNKNOWN = 99;

}
