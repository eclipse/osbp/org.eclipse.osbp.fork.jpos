/*
 *                                                                            
 *  Copyright (c) 2011 - 2016 - Loetz GmbH & Co KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License v1.0       
 *  which accompanies this distribution, and is available at                  
 *  http://www.eclipse.org/legal/epl-v10.html                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG
 * 
 */
package jpos.loader;

import java.util.Hashtable;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;

/**
 * The JposServiceBinder to concrete vendor implementations.
 * Please provide a vendor specific implementation of interface {@link JposServiceInstanceFactory}.
 * Simply create a plug-in bundle containing all vendor jars in a folder, 
 * export all necessary packages and supply a component description in the OSGI-INF folder that
 * provides the service of interface {@link JposServiceInstanceFactory}.
 * For Epson the component xml looks like this:
 * <?xml version="1.0" encoding="UTF-8"?>
 * <scr:component xmlns:scr="http://www.osgi.org/xmlns/scr/v1.1.0" name="jp.co.epson.uposcommon.creator.EpsonJposServiceInstanceFactory">
 * <implementation class="jp.co.epson.uposcommon.creator.EpsonJposServiceInstanceFactory"/>
 * <service>
 *    <provide interface="jpos.loader.JposServiceInstanceFactory"/>
 * </service>
 * </scr:component>
 */
@Component
public class JposServiceBinder {

	/** The log. */
	private static Logger log = org.slf4j.LoggerFactory
			.getLogger(jpos.loader.JposServiceBinder.class);

	/** The si instance factory map. */
    private static Hashtable<String, JposServiceInstanceFactory> siFactoryTable = new Hashtable<>();

	/**
	 * Bind factory service.
	 *
	 * @param siInstanceFactory
	 *            the serviceInstanceFactory
	 */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	public synchronized void bindFactoryService(final JposServiceInstanceFactory siInstanceFactory) {
		JposServiceBinder.siFactoryTable.put(siInstanceFactory.getClass().getCanonicalName(), siInstanceFactory);
		log.debug("Jpos ServiceInstanceFactory "+siInstanceFactory.getClass().getCanonicalName()+" added");
	}

	/**
	 * Unbind factory service.
	 *
	 * @param siInstanceFactory
	 *            the serviceInstanceFactory
	 */
	public synchronized void unbindFactoryService(
			final JposServiceInstanceFactory siInstanceFactory) {
		if(JposServiceBinder.siFactoryTable.contains(siInstanceFactory)) {
			JposServiceBinder.siFactoryTable.remove(siInstanceFactory.getClass().getCanonicalName());
		}
		log.debug("Jpos ServiceInstanceFactory "+siInstanceFactory.getClass().getCanonicalName()+" removed");
	}

	/**
	 * Gets the serviceInstanceFactory.
	 *
	 * @return serviceInstanceFactory
	 */
	public static Hashtable<String, JposServiceInstanceFactory> getSiFactoryTable() {
		return JposServiceBinder.siFactoryTable;
	}

}
